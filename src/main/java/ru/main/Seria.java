package ru.main;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Viktor on 30.07.2015.
 */
public class Seria {

    private Long seria_id;
    private String seria_name;


    public Seria(){
    }

    public Seria(Long seria_id, String seria_name){
        this.seria_id = seria_id;
        this.seria_name = seria_name;
    }

    public Seria(ResultSet rs)  throws SQLException {
        this.seria_id = rs.getLong("seria_id");
        this.seria_name = rs.getString("seria_name");
    }



    public Long getSeria_id() {
        return seria_id;
    }

    public void setSeria_id(Long seria_id) {
        this.seria_id = seria_id;
    }

    public String getSeria_name() {
        return seria_name;
    }

    public void setSeria_name(String seria_name) {
        this.seria_name = seria_name;
    }


}
