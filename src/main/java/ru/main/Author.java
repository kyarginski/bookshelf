package ru.main;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Viktor on 30.07.2015.
 */
public class Author {
    private Long author_id;
    private String first_name;
    private String surname;
    private int birth_year;
    private int death_year;
    private String birth_place;

    public Author(){
    }

    public Author(Long author_id, String first_name, String surname, int birth_year, int death_year, String birth_place){
        this.author_id = author_id;
        this.first_name = first_name;
        this.surname = surname;
        this.birth_year = birth_year;
        this.death_year = death_year;
        this.birth_place = birth_place;
    }

    public Author(ResultSet rs)  throws SQLException {
        this.author_id = rs.getLong("author_id");
        this.first_name = rs.getString("first_name");
        this.surname = rs.getString("surname");
        this.birth_year = rs.getInt("birth_year");
        this.death_year = rs.getInt("death_year");
        this.birth_place = rs.getString("birth_place");
    }


    public Long getAuthor_id() {
        return author_id;
    }

    public void setAuthor_id(Long author_id) {
        this.author_id = author_id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getBirth_year() {
        return birth_year;
    }

    public void setBirth_year(int birth_year) {
        this.birth_year = birth_year;
    }

    public int getDeath_year() {
        return death_year;
    }

    public void setDeath_year(int death_year) {
        this.death_year = death_year;
    }

    public String getBirth_place() {
        return birth_place;
    }

    public void setBirth_place(String birth_place) {
        this.birth_place = birth_place;
    }


}
