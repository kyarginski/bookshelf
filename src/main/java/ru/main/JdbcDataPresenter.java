package ru.main;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;


/**
 * Created by Viktor on 11.08.2015.
 */
public class JdbcDataPresenter {

    private static final Logger log = LoggerFactory.getLogger(JdbcDataPresenter.class);

//    private static final Logger log = Logger.getLogger(JdbcDataPresenter.class);

    private static JdbcDataPresenter instance;

    public JdbcDataPresenter() {
    }

    public static synchronized JdbcDataPresenter getInstance() throws Exception {
        if (instance == null) {
            instance = new JdbcDataPresenter();
        }
        return instance;
    }


    private Connection getConnection() throws ClassNotFoundException, SQLException {
        Class.forName("org.postgresql.Driver");
        String url = "jdbc:postgresql://localhost:5432/postgres";
        Connection con = DriverManager.getConnection(url, "postgres", "postgres");

        log.debug("Get connected: "+ url);
        return con;
    }

    public List<String> getBooksHeaders() {
        //можно получать из БД
        List<String> head = new ArrayList<String>();
        head.add("№");
        head.add("ID книги");
        head.add("Наименование книги");
        head.add("Автор");
        head.add("Серия");
        head.add("Год создания");
        return head;
    }


    public List<Book> getAllBooks() {
        List<Book> BookList = new LinkedList<Book>();

        try {
            Connection con = getConnection();
            try {
                String sql = "SELECT b.*, concat(a.surname,' ',a.first_name) author_name, s.seria_name  \n" +
                        "FROM \n" +
                        "  jc.jc_books b LEFT JOIN jc.jc_authors a ON b.author_id=a.author_id\n" +
                        "  LEFT JOIN jc.jc_series s ON b.seria_id=s.seria_id\n" +
                        "ORDER BY Book_Name";
                PreparedStatement stmt = con.prepareStatement(sql);

                ResultSet rs = stmt.executeQuery();

                while (rs.next()) {
                    Book g = new Book(rs);
                    BookList.add(g);
                }
                log.info("Read info about "+BookList.size()+" books");

                rs.close();
                stmt.close();
            } finally {
                con.close();
            }
        } catch (ClassNotFoundException ex) {
            log.error("Ошибка",ex);
            ex.printStackTrace();
        } catch (SQLException ex) {
            log.error("Ошибка",ex);
            ex.printStackTrace();
        }

        return BookList;

    }

    public List<String> getSeriesHeaders() {
        //можно получать из БД
        List<String> head = new ArrayList<String>();
        head.add("№");
        head.add("ID серии");
        head.add("Наименование серии");
        return head;
    }


    public List<Seria> getAllSeries() {
        List<Seria> SeriaList = new LinkedList<Seria>();

        try {
            Connection con = getConnection();
            try {
                String sql = "SELECT * FROM jc.jc_Series ORDER BY Seria_Name";
                PreparedStatement stmt = con.prepareStatement(sql);
                ResultSet rs = stmt.executeQuery();
                while (rs.next()) {
                    Seria ser = new Seria(rs);
                    SeriaList.add(ser);
                }
                log.info("Read info about "+SeriaList.size()+" series" );
                rs.close();
                stmt.close();
            } finally {
                con.close();
            }
        } catch (ClassNotFoundException ex) {
            log.error("Ошибка",ex);
            ex.printStackTrace();
        } catch (SQLException ex) {
            log.error("Ошибка",ex);
            ex.printStackTrace();
        }

        return SeriaList;

    }

    public List<String> getAuthorsHeaders() {
        //можно получать из БД
        List<String> head = new ArrayList<String>();
        head.add("№");
        head.add("ID автора");
        head.add("ФИО");
        head.add("Год рождения");
        head.add("Год смерти");
        head.add("Место рождения");
        return head;
    }


    public List<Author> getAllAuthors() {
        List<Author> AuthorList = new LinkedList<Author>();

        try {
            Connection con = getConnection();
            try {
                String sql = "SELECT * FROM jc.jc_authors ORDER BY surname";
                PreparedStatement stmt = con.prepareStatement(sql);
                ResultSet rs = stmt.executeQuery();
                while (rs.next()) {
                    Author author = new Author(rs);
                    AuthorList.add(author);
                }
                log.info("Read info about "+AuthorList.size()+" authors");
                rs.close();
                stmt.close();
            } finally {
                con.close();
            }
        } catch (ClassNotFoundException ex) {
            log.error("Ошибка",ex);
            ex.printStackTrace();
        } catch (SQLException ex) {
            log.error("Ошибка",ex);
            ex.printStackTrace();
        }

        return AuthorList;
    }
}
