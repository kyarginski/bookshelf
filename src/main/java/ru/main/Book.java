package ru.main;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Viktor on 29.07.2015.
 */
public class Book {
    private Long book_id;
    private String book_name;
    private Long author_id;
    private String author_name;
    private Long seria_id;
    private String seria_name;
    private int create_year;
//    List<Author> author;
//    List<Seria> seria;

    public String getSeria_name() {
        return seria_name;
    }

    public void setSeria_name(String seria_name) {
        this.seria_name = seria_name;
    }

    public String getAuthor_name() {
        return author_name;
    }

    public void setAuthor_name(String author_name) {
        this.author_name = author_name;
    }

    public Book() {
    }

    public Book(Long book_id, String book_name, Long author_id, String author_name, Long seria_id, String seria_name, int create_year) {
        this.book_id = book_id;
        this.book_name = book_name;
        this.author_id = author_id;
        this.author_name = author_name;
        this.seria_id = seria_id;
        this.seria_name = seria_name;
        this.create_year = create_year;
    }

    public Book(ResultSet rs)  throws SQLException {
        this.book_id = rs.getLong("book_id");
        this.book_name = rs.getString("book_name");
        this.author_id = rs.getLong("author_id");
        this.author_name = rs.getString("author_name");
        this.seria_id = rs.getLong("seria_id");
        this.seria_name = rs.getString("seria_name");
        this.create_year = rs.getInt("create_year");
    }


    public Long getBook_id() {
        return book_id;
    }

    public void setBook_id(Long book_id) {
        this.book_id = book_id;
    }

    public String getBook_name() {
        return book_name;
    }

    public void setBook_name(String book_name) {
        this.book_name = book_name;
    }

    public Long getAuthor_id() {
        return author_id;
    }

    public void setAuthor_id(Long author_id) {
        this.author_id = author_id;
    }

    public Long getSeria_id() {
        return seria_id;
    }

    public void setSeria_id(Long seria_id) {
        this.seria_id = seria_id;
    }

    public int getCreate_year() {
        return create_year;
    }

    public void setCreate_year(int create_year) {
        this.create_year = create_year;
    }

}
