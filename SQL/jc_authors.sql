﻿-- Table: jc.jc_authors

-- DROP TABLE jc.jc_authors;

CREATE TABLE jc.jc_authors
(
  author_id bigserial NOT NULL,
  first_name character varying(50),
  surname character varying(50),
  birth_year integer,
  death_year integer,
  birth_place character varying(50),
  CONSTRAINT jc_authors_pkey PRIMARY KEY (author_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE jc.jc_authors
  OWNER TO postgres;
COMMENT ON TABLE jc.jc_authors
  IS 'Авторы';
