﻿-- Table: jc.jc_series

-- DROP TABLE jc.jc_series;

CREATE TABLE jc.jc_series
(
  seria_id bigserial NOT NULL,
  seria_name character varying(50),
  CONSTRAINT jc_series_pkey PRIMARY KEY (seria_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE jc.jc_series
  OWNER TO postgres;
COMMENT ON TABLE jc.jc_series
  IS 'Серии книг';
