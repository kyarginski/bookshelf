﻿-- Table: jc.jc_books

-- DROP TABLE jc.jc_books;

CREATE TABLE jc.jc_books
(
  book_id bigserial NOT NULL,
  book_name character varying(250),
  author_id bigint,
  seria_id bigint,
  create_year integer,
  CONSTRAINT jc_books_pkey PRIMARY KEY (book_id),
  CONSTRAINT jc_book_author_fk FOREIGN KEY (author_id)
      REFERENCES jc.jc_authors (author_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT jc_book_serie_fk FOREIGN KEY (seria_id)
      REFERENCES jc.jc_series (seria_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE jc.jc_books
  OWNER TO postgres;
COMMENT ON TABLE jc.jc_books
  IS 'Книжки';
