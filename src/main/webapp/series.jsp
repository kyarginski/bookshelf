<%@ page import="ru.main.Book" %>
<%@ page import="java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%-- Все варианты библиотеки JSTL --%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"%>
<%@taglib prefix="x" uri="http://java.sun.com/jsp/jstl/xml"%>
<%@taglib prefix="i18n" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>


<html>
  <head>
    <title>Bookshelf</title>
  </head>
  <body>
  <h1>Bookshelf - книжная полка</h1>

  <p><a href="${pageContext.request.contextPath}/authors">Авторы в алфавитном порядке</a></p>
  <p><a href="${pageContext.request.contextPath}/books">Книги в алфавитном порядке</a></p>
  <h2>Серии книг</h2>

  <jsp:useBean id="bookPresenter" scope="application" class="ru.main.JdbcDataPresenter"/>

  <table border="1">
    <thead>
    <tr>
      <c:forEach var="head" items="${headList}">
        <th>${head}</th>
      </c:forEach>
    </tr>
    </thead>
    <tbody>
    <c:forEach var="author" items="${seriaList}" varStatus="row_num">

      <c:if test="${row_num.count%2==1}">
        <tr style="background-color: lightskyblue;">
      </c:if>
      <c:if test="${row_num.count%2!=1}">
        <tr style="background-color: white;">
      </c:if>

      <td>${row_num.count}</td>
      <td>${author.seria_id}</td>
      <td>${author.seria_name}</td>

      </tr>
    </c:forEach>
    </tbody>
  </table>


  </body>
</html>
