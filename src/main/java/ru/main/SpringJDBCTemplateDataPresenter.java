package ru.main;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Victor on 21.08.2015.
 */
public class SpringJDBCTemplateDataPresenter {

    private static final Logger log = LoggerFactory.getLogger(JdbcDataPresenter.class);

    private static SpringJDBCTemplateDataPresenter instance;
    private static JdbcTemplate jdbc;


    public SpringJDBCTemplateDataPresenter(String param_file) {
        ApplicationContext context = new ClassPathXmlApplicationContext(new String[]{param_file});
        jdbc = context.getBean("jdbcTemplate", JdbcTemplate.class);
    }

    public static synchronized SpringJDBCTemplateDataPresenter getInstance() throws Exception {
        if (instance == null) {
            instance = new SpringJDBCTemplateDataPresenter("springContext.xml");
        }
        return instance;
    }

    public List<String> getBooksHeaders() {
        //можно получать из БД
        List<String> head = new ArrayList<String>();
        head.add("№");
        head.add("ID книги");
        head.add("Наименование книги");
        head.add("Автор");
        head.add("Серия");
        head.add("Год создания");
        return head;
    }


    public List<Book> getAllBooks()  throws SQLException  {
        List<Book> BookList = new LinkedList<Book>();
        String sql = "SELECT b.*, concat(a.surname,' ',a.first_name) author_name, s.seria_name  \n" +
                "FROM \n" +
                "  jc.jc_books b LEFT JOIN jc.jc_authors a ON b.author_id=a.author_id\n" +
                "  LEFT JOIN jc.jc_series s ON b.seria_id=s.seria_id\n" +
                "ORDER BY Book_Name";

        BookList = jdbc.query(sql, new RowMapper<Book>() {
            @Override
            public Book mapRow(ResultSet rs, int i) throws SQLException {
            Book book = new Book(rs);
                return book;
            }
        });

        log.info("Вычитали из базы "+BookList.size()+" книг");
        return BookList;

    }


    public List<String> getSeriesHeaders() {
        //можно получать из БД
        List<String> head = new ArrayList<String>();
        head.add("№");
        head.add("ID серии");
        head.add("Наименование серии");
        return head;
    }

    public List<Seria> getAllSeries() {
        List<Seria> SeriaList = new LinkedList<Seria>();
        String sql = "SELECT * FROM jc.jc_Series ORDER BY Seria_Name";

        SeriaList = jdbc.query(sql, new RowMapper<Seria>() {
            @Override
            public Seria mapRow(ResultSet rs, int i) throws SQLException {
                Seria seria = new Seria(rs);
                return seria;
            }
        });

        log.info("Отгрузили из базы "+SeriaList.size()+" серий");

        return SeriaList;

    }

    public List<String> getAuthorsHeaders() {
        //можно получать из БД
        List<String> head = new ArrayList<String>();
        head.add("№");
        head.add("ID автора");
        head.add("ФИО");
        head.add("Год рождения");
        head.add("Год смерти");
        head.add("Место рождения");
        return head;
    }


    public List<Author> getAllAuthors() {
        List<Author> AuthorList = new LinkedList<Author>();
        String sql = "SELECT * FROM jc.jc_authors ORDER BY surname";

        AuthorList = jdbc.query(sql, new RowMapper<Author>() {
            @Override
            public Author mapRow(ResultSet rs, int i) throws SQLException {
                Author author = new Author(rs);
                return author;
            }
        });

        log.info("Подтянули из базы "+AuthorList.size()+" авторов");

        return AuthorList;
    }

}
