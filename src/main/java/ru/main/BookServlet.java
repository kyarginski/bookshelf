package ru.main;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created by Viktor on 29.07.2015.
 */
public class BookServlet extends HttpServlet {

//    private static final Logger log = Logger.getLogger(BookServlet.class);
    private static final Logger log = LoggerFactory.getLogger(BookServlet.class);

    protected void processRequest(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        List<String> head;
        try {
//            head = JdbcDataPresenter.getInstance().getBooksHeaders();
            head = SpringJDBCTemplateDataPresenter.getInstance().getBooksHeaders();
            req.setAttribute("headList", head);
        } catch (Exception e) {
            log.error("Ошибка получения заголовков",e);
            e.printStackTrace();
        }

        List<Book> books = null;
        try {
//            books = JdbcDataPresenter.getInstance().getAllBooks();
            books = SpringJDBCTemplateDataPresenter.getInstance().getAllBooks();
        } catch (Exception e) {
            log.error("Ошибка получения данных о книгах",e);
            e.printStackTrace();
        }

        req.setAttribute("bookList", books);
        getServletContext().getRequestDispatcher("/books.jsp").forward(req,resp);

        // Другое отображение вызывается заменой JSP
        //getServletContext().getRequestDispatcher("/authors.jsp").forward(req, resp);

    }


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

}
