<%@ page import="ru.main.Book" %>
<%@ page import="java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%-- Все варианты библиотеки JSTL --%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"%>
<%@taglib prefix="x" uri="http://java.sun.com/jsp/jstl/xml"%>
<%@taglib prefix="i18n" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>


<html>
  <head>
    <title>Bookshelf</title>
  </head>
  <body>
  <h1>Bookshelf - книжная полка</h1>

  <h2>Авторы в алфавитном порядке</h2>
  <p><a href="${pageContext.request.contextPath}/books">Книги в алфавитном порядке</a></p>
  <p><a href="${pageContext.request.contextPath}/series">Серии книг</a></p>

  <jsp:useBean id="bookPresenter" scope="application" class="ru.main.JdbcDataPresenter"/>

  <table border="1">
    <thead>
    <tr>
      <c:forEach var="head" items="${headList}">
        <th>${head}</th>
      </c:forEach>
    </tr>
    </thead>
    <tbody>
    <c:forEach var="author" items="${authorList}" varStatus="row_num">

      <c:if test="${row_num.count%2==1}">
        <tr style="background-color: lightskyblue;">
      </c:if>
      <c:if test="${row_num.count%2!=1}">
        <tr style="background-color: white;">
      </c:if>

      <td>${row_num.count}</td>
      <td>${author.author_id}</td>
      <td>${author.surname} ${author.first_name}</td>
      <td>${author.birth_year}</td>
      <td>${author.death_year}</td>
      <td>${author.birth_place}</td>


      </tr>
    </c:forEach>
    </tbody>
  </table>


  </body>
</html>
