package ru.main;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created by Viktor on 29.07.2015.
 */
public class SeriaServlet extends HttpServlet {

//    private static final Logger log = Logger.getLogger(BookServlet.class);
    private static final Logger log = LoggerFactory.getLogger(SeriaServlet.class);

    protected void processRequest(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        List<String> head;
        try {
//            head = JdbcDataPresenter.getInstance().getSeriesHeaders();
            head = SpringJDBCTemplateDataPresenter.getInstance().getSeriesHeaders();
            req.setAttribute("headList", head);
        } catch (Exception e) {
            log.error("Ошибка получения заголовков",e);
            e.printStackTrace();
        }

        List<Seria> series = null;
        try {
//            series = JdbcDataPresenter.getInstance().getAllSeries();
            series = SpringJDBCTemplateDataPresenter.getInstance().getAllSeries();
        } catch (Exception e) {
            //TODO добавить логирование
            e.printStackTrace();
        }

        req.setAttribute("seriaList", series);
        getServletContext().getRequestDispatcher("/series.jsp").forward(req,resp);

    }


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

}
