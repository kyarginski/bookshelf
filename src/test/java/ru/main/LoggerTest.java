package ru.main;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by Viktor on 13.08.2015.
 */
public class LoggerTest {

//    private static final Logger log = Logger.getLogger(LoggerTest.class);
    private static final Logger log = LoggerFactory.getLogger(LoggerTest.class);


    @Test
    public void test() {
        //all, trace, debug, info, warn, error, fatal
        log.trace("trace message");
        log.debug("debug message");
        log.info("info message");
        log.warn("warn message");
        log.error("error message");
//        log.fatal("fatal message");
    }
}
