<%@ page import="ru.main.Book" %>
<%@ page import="java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%-- Все варианты библиотеки JSTL --%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"%>
<%@taglib prefix="x" uri="http://java.sun.com/jsp/jstl/xml"%>
<%@taglib prefix="i18n" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>


<html>
  <head>
    <title>Bookshelf</title>
  </head>
  <body>
  <h1>Bookshelf - книжная полка</h1>

  <p><a href="${pageContext.request.contextPath}/authors">Авторы в алфавитном порядке</a></p>
  <p><a href="${pageContext.request.contextPath}/books">Книги в алфавитном порядке</a></p>
  <p><a href="${pageContext.request.contextPath}/series">Серии книг</a></p>



  </body>
</html>
